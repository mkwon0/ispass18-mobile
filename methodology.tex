\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{figs/over_high.eps}
\caption{High level-view of \texttt{MobileSSD}.\vspace{-8pt}}
\label{fig:over_high}
\end{figure}

Figure \ref{fig:over_high} shows storage stack that exists from an user application to the mobile storage, and illustrates the components that \texttt{MobileSSD} supports to enable full system simulations in handheld computing as a high-level view. A user application can request an I/O to a virtual file system (VFS) through system call (syscall). The VFS buffers the incoming I/O request if it needs into a page cache, and generates the corresponding block request. The block request is then forwarded to a native file system; in our implementation, we use ext4 \cite{cao2007ext4} as the native file system. The file system then forward request to block layer, which scheduled by I/O scheduler or blkmq. If the system employs UFS, the block layer forward the request to SCSI mid layer and SCSI low-level driver that includes the UFS kernel driver. If the underlying storage uses NVMe protocol, the block layer sends the request to the NVMe kernel driver\footnote{Note that, while previous version of NVMe driver bypass the block layer by managing queues itself, after Linux kernel 3.19, NVMe driver uses blkmq rather than bypassing \cite{bjorling2013linux}.} The request is passed to process by the components of our \texttt{MobileSSD}, which is highlighted at Figure \ref{fig:over_high}.

%% LevelDB등을 테스트할 때는 ext4를 사용하였습니다만, FIO에서는 filesystem없이 block device로 I/O test를 하였습니다.

While all components of NVMe side (e.g., PCIe endoint and NVMe controller) are included at the device side, UFS in our implementation is spared across both host and device sides. UTP engine and UIC manager are required to not only for physical communication but also for software protocol management.

\subsection{UFS Simulation Components}
\noindent \textbf{UIC manager.}
The UIC manager is abstracted module of DME, Device Management Engine, which manages UniPro PHY layers.
In reality, DME initialize links between UFS Host and UFS device, handling data communication and managing power consumption.
However, we are on simulation, not on real hardware, so we omit unnecessary parts such as data encoding and link establishment.
We modeled only data communication with constant value to limit bandwidth of UniPro PHY.

\noindent \textbf{UTP engine.}
The UTP engine handles all UTP Transfer/Task commands.
On real hardware, UTP engine exists on both UFS Host and UFS Device.
This is because when UFS Host got UTP commands from UFS Driver (i.e., Host), host side UTP engine bypass or reorganize UTP commands and sends to UTP Device.
% UTP command protocol is used on both 'UTP Driver <-> UTP Host communication' and 'UTP Host <-> UTP Device communication'
As we explained in previous section, host side UTP engine decompose one UTP command (SCSI) into several small UTP commands (DATA IN/OUT).
These decomposed UTP commands are handled by device side UTP engine.
For implementation convenience, we removed device side UTP engine. Host side UTP engine parse the UTP commands as real hardware while pass the commands to UTP device as simple function call.

\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{figs/impl_UIC.eps}
\caption{UFS InterConnect (UIC) command protocol.\vspace{-15pt}}
\label{fig:impl_UIC}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{figs/impl_UTP.eps}
\caption{UFS Transfer Protocol (UTP) transfer request.\vspace{-15pt}}
\label{fig:impl_UTP}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{figs/impl_struct.eps}
\caption{Details of UFS structure.\vspace{-15pt}}
\label{fig:impl_struct}
\end{figure}


\subsection{UFS Implementation Details}
\noindent \textbf{UFS Host Controller}
Figure \ref{fig:impl_UIC} pictorially explains the UFS InterConnect (UIC) command protocol, which is defined by UFSHCI specification \cite{UFSHCI}. The UFS host driver records relevant arguments and the corresponding command code to UIC register, UIC manager performs the I/O services associated with the command code. Once the I/O service is completed, it returns the target values and reports whether the request has bee served in success by updating argument register. In addition, the manager updates the completion flag observed in the interrupt status register.
After this report progress, the UIC manager signals the interrupt to the host, the host-side OS kernel tries to read the argument registers and finish the I/O completion by resetting the flag of such interrupt status register.
% 본 논문에서 구현된 UIC manager는 UFS PHY가 kernel에 의해 정상적으로 동작한다고 인식할 수 있는 최소의 동작만이 구현되어 있다.

As shown in Figure \ref{fig:impl_struct}, the TCQ managed by UFS has 32 queue entries, each containing the corresponding UTP request descriptor. In this UTP request descriptor, there is command type that explains whether the target request is UTP transfer or UTP task, and UTP command descriptor's base address that maintains the details of request information. In addition the descriptor defines where the requests should be stored (i.e., UPIU response offset) and where the data should be read and stored into (i.e., PRDT offset).
On the other hand, UTP transfer is used for transferring the data, while UTP task is employed to request actual a series of I/O tasks to the underlying device. 
The UFS command descriptor of UFS transfer request consists of UFS Protocol Information Units (UPIU) transfer request, UPIU transfer response, and Physical Regions Description Table (PRDT). The UTP engine of UFS host controller parses the corresponding UPIU transfer request, and forwards it to UFS device's UTP engine. In addition, UFS device records the results of UTP engine into UPIU transfer response.


Figure \ref{fig:impl_UTP} illustrates the process of UFS host driver to issue a UTP command. Specifically, UFS host driver stores the UTP request descriptor into an available queue entry of TCQ, and then set the corresponding slot bit-number  to 1 by updating the underlying UTP transfer doorbell register. After then, UFS host controller checks whether there exist a new command arrival in which number of TCQ slot, it forwards the index number of such slot to UTP engine. The UTP engine start to process the command as explained in previous. 

\noindent \textbf{UFS Device}
UTP engine of the underlying UFS Device retrieves the information regarding the UPIU transfer request, which has been parsed by UFS Host controller, and then it calls the command handler based on which OPCODE is requested (related to SCSI, Native UFS, Device Management Command. Specifically, SCSI command contains the information regarding SCSI protocol, which defines all required SCSI commands that should be supported to map UFS device to a SCSI block device. The Device Management Command is used for bring the UFS device information, which cannot be delivered by SCSI command such as a device serial number. 

\subsection{Enabling NVMe}
To enable NVMe interface, we modify the interrupt handling routine and PCI device implemented in gem5. 

\noindent \textbf{PCI modification}
Since there is no PCIe implementation in gem5, we leverage PCI in enabling PCIe physical interface, but modify the latency of  physical interface data movement. Since the current OS only uses the address of \texttt{bus:device.function} for both PCI and PCIe devices, our PCI modification is compatible with the operations of existing PCIe interface. However, one of the challenges of PCI modification is that the current gem5 has a lack of mechanisms related to MSI/MSI-X that should be supported to enable NVMe. We implement MSI/MSI-X by incorporating about 300 lines code that activates MSI/MSI-X and the corresponding interface table when the host writes MSI/MSI-X configuration area. In addition, our implementation includes the  signaling mechanisms that interrupts the host when the underlying NVMe controller requests an interrupt post, based on the current gem5's interrupt routine. 


\noindent \textbf{Device Tree modification}
While ARM architecture implemented in gem5 has Generic Interrupt Controller (GIC) v2m, which may used for handling the MSI/MSI-X interrupt, unfortunately, it cannot support the device tree of VExpress\_GEM5\_V1 machine, which is required by Linux kernel 4.x. Thus, the Linux kernel cannot identify MSI controller of the PCI host controller, which in turn makes MSI/MSI-X unable to use from the host viewpoint. To address this problem, we explicitly declare the existence of msi-controller in the PCI host controller by adding GICv2m into the device tree code. We also use Linux kernel's Open Firmware interface to initialize GICv2m and enable the functions of MSI/MSI-X interrupt regarding our modified PCI interface. 

%\noindent \textbf{System Crossbar modification}
% MJ: I temperally commented this out. 
%% 이 부분은 UFS가 동작하지 않는 x86 architecture에 대한 내용이어서 지워도 될 듯 합니다.
% gem5의 x86 arcitecture에서 MSI/MSI-X interrupt를 사용하기 위해서는 north bridge에 MSI frame을 각 CPU의 Local Advanced Programmable Interrupt Controller (LAPIC)로 route해주는 memory address router를 구현해야 한다.
% Intel MP specification \ref{} 와 Intel 64 and IA-32 Architectures Software Developers Manual \ref{}에 정의되어 있는 MSI/MSI-X interrupt address range와 data specification을 참고하여 System Crossbar에 특정 memory address에 대한 I/O access를 CPU의 LAPIC으로 route하는 routine을 작성하여 MSI/MSI-X가 동작하도록 수정하였다.

%% 더 자세하게 쓸 수 있습니다. (아마 그림이 더 필요할 것입니다.)