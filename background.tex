In this section, we will explain how UFS and NVMe storage media can be connected to CPU cores based on the architecture specification of Advanced RISC Machines (ARM). We will also describe the queue management and interrupt handing mechanisms for UFS and NVMe.


\begin{figure}
	\subfloat[UFS.\label{fig:back_arch_u}]{%
		\includegraphics[width=0.49\columnwidth]{figs/back_arch_u.eps}
	}
	\subfloat[NVMe.\label{fig:back_arch_n}]{
		\includegraphics[width=0.49\columnwidth]{figs/back_arch_n.eps}
	}
	\vspace{-8pt}
	\caption{Overall architecture.\vspace{-15pt}}
	\label{fig:back_arch}
\end{figure}

\begin{figure}
	\subfloat[UFS.\label{fig:back_queue_u}]{%
		\includegraphics[width=0.49\columnwidth]{figs/back_queue_u.eps}
	}
	\subfloat[NVMe.\label{fig:back_queue_n}]{
		\includegraphics[width=0.49\columnwidth]{figs/back_queue_n.eps}
	}
	\caption{Queue management comparison.\vspace{-15pt}}
	\label{fig:back_queue}
\end{figure}

\begin{figure}
	\subfloat[UFS.\label{fig:back_intr_u}]{%
		\includegraphics[width=0.49\columnwidth]{figs/back_intr_u.eps}
}
	\subfloat[NVMe.\label{fig:back_intr_n}]{
		\includegraphics[width=0.49\columnwidth]{figs/back_intr_n.eps}
	}
	\caption{Interrupt handling comparison.\vspace{-15pt}}
	\label{fig:back_intr}
\end{figure}

\subsection{Datapath for Mobile Storage}
\noindent \textbf{UFS.} Figure \ref{fig:back_arch} illustrates the overall architecture of UFS and show the corresponding datapath. An ARM processor employs an on-chip UFS host controller, which can communicate with the underlying UFS device through an UFS InterConnect. The UFS host controller is connected over a system bus, and therefore a file system or a UFS kernel driver can request I/Os via a UFS host controller interface. Both UFS host and device employs a UTP engine and a UIC manager. The UIC manager establish the link between those two UFS controller and manage UIC physical layer. Atop UIC, the UTP engine handles requests, which defines tagged command queueing (TCQ) and SCSI/native UFS command specifications. The UFS device parses SCSI commands and forward them to the underlying flash media through flash firmware that also connect such media over Open NAND Flash interface (ONFi). Note that all kernels on operating system (OS) cannot directly communicate with the underlying flash; always, the request should be submitted to the UFS host controller, and then the controller communicate with the UFS device interface, which can increase I/O latency.

\noindent \textbf{NVMe.} In contrast to UFS, NVMe has no host side controller on its datapath. As shown in Figure \ref{fig:back_arch}, an NVMe kernel driver can access PCIe root port, which implements PCIe physical layer (PHY) and connects the SSD as a PCIe endpoint. Specifically, the SSD exposes storage configurations and memory regions related to its protocol interface through PCIe base address registers (BARs), which are mapped to the host's system memory space. Thus, the NVMe kernel driver reads and writes the BARs to compose NVMe commands and the corresponding packets, which in turn allows the NVMe driver directly control and access the underlying storage. Inside the SSD, an NVMe controller parses NVMe commands, which were written to PCIe BAR(s), and manages rich NVMe queues (which will be explained shortly). The parsed I/O requests are forwarded to the flash firmware and serviced by the underlying flash media through ONFi. Note that, while each UIC lane (of UFS) is capable of supporting 5.8 Gbit/s (HS-G3 \cite{HSG3}), PCIe lane can support 8.0 Gbit/s (PCIe 3.0 \cite{ajanovic2009pci}). While UFS uses two lanes for communication, PCIe can be configured to use up to 16 lanes, which in turn can theoretically bring 11 times better performance (without data encoding).


\subsection{Queue Management and Data Transfers}
\noindent \textbf{UFS.} Since the UTP engines support SCSI command sets and protocols, UFS kernel driver can queue the requests that target the UFS mobile storage by using tagged command queue (TCQ). The support of TCQ is a notable difference between UFS and other mobile storage interfaces such as eMMC, since eMMC has no queuing mechanism (e.g., every request is treated as blocking I/O services). TCQ allows the host UTP engine to enqueue I/O requests upto 32, which can enable true asynchronous I/O operations. Each request within TCQ has a tag that contains request-specific information such as logical block address and request length, but not for actual data. The target data resides on the host side system memory, which is indicated by a physical region description table (PRDT). The UFS device interface (within an SSD) can initiate requests based on its availability and complete them as an out-of-order fashion.

\noindent \textbf{NVMe.} NVMe queue support multiple queues (upto 65536), each is capable of enqueuing 65536 I/O requests. This rich queue mechanism allows the underlying flash firmware can maximize the degree of SSD internal parallelism, thereby high-bandwidth. To reduce the overheads imposed by communications between the host and the SSD, the NVMe queue mechanism employs submission queue (SQ) and completion queue (CQ) for each queue as a pair, each being managed by head and tail references. The head and tail indicates the ending point and the starting point of outstanding requests, respectively. Thus, from the NVMe driver viewpoint, all requests are processed by FIFO out-of-order I/O scheduling, but the NVMe driver can complete requests and update CQ in an out-of-order if it needs. Similar to the scheme that UFS's PRDT supports, the actual data reside in the host system memory, which is referred by physical region page (PRP) lists. Once the NVMe driver process a request from SQ, the corresponding data is transferred from the host to the SSD through DMA (and this data transfer is performed for CQ in a similar manner).


\subsection{I/O Completion}
% Figure fig:back_intr should be changed
% UFS interrupt is posted by UFS host, not UFS device
% NVME:
% 0. Assign unique interrupt vector per CQ
% 1. MSI/MSI-X
%
% [Core 0] [Core 1] ... [Core N-1]
% [ CQ 1 ] [ CQ 2 ] ... [  CQ N  ] (CQ 0 is admin queue, on Core 0)
%   | 3. Process I/O completion
% [           NVMe Driver        ]
%   | 2. NVMe Interrupt handler called
% [ Platform specific MSI controller ] // NVMe driver requests unique interrupt vector to MSI controller and assign them to each CQ
%   | 1. Post MSI/MSI-X
% [          NVMe device         ]

Figure \ref{fig:back_intr} shows I/O completion routine of both UFS and NVMe.

\noindent \textbf{UFS.}
When host side UTP engine fetch the new SCSI I/O command from TCQ, it decompose request into small data chunks.
These decomposed chunks are send to device side UTP engine and UTP device interface handles I/O on NVM media.
Then UTP device interface sends ACK to host side UTP engine so the host side UTP engine can send next data chunks.
% See Section 10.7.4. and 10.7.13. of UFS Specification v2.1 for more details
% These data communication between host side UTP engine and device side UTP engine should be in order communication.
% The maximum data size of one data chunk is 65535bytes (= 127 logical blocks when block size is 512 bytes)
% There is no exact reason that decompose request into small chunks on specification.
% But they may want to parallelize NVM I/O and DMA operation. (send/receive data chunk while read/write next chunk)
After transmission of data chunks are done, host side UTP engine writes results to tag of TCQ and post interrupt.
When an interrupt handler of UTP driver called, it reads the tag of finished I/O and request completion of I/O to SCSI driver.

\noindent \textbf{NVMe.}
To maximize performance, blkmq \cite{bjorling2013linux} allocates multiple I/O Completion Queues (CQs), usually same number of CPUs/Cores.
NVMe driver assign different interrupt vector to each CQ to quickly determine which CQ contains completed requests.
When NVMe controller got new request from SQ, it enqueue the request to internal FIFO queue in arbitration method selected by the NVMe driver.
Then controller dequeue the request and handle I/O on NVM media.
After I/O, controller write response to CQ associated with SQ that request coming from.
And controller generates interrupt with assigned interrupt vector of CQ.
When an interrupt handler of NVMe driver called, it checks the interrupt vector to determine which CQ has outstanding response and request completion of I/O to blkmq.
