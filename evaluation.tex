To explore the full design space of handheld computing system, we first model an NVMe SSD and UFS-based SSD and integrate them in a full system simulator (gem5) by revising the I/O interface and protocol. Our new full system simulator not only implements a full firmware stack and actual NVMe and SCSI interface, but also can be reconfigurable being aware of SSD internal resources. The proposed simulator enables to simulate real-world benchmarks on a real Linux system and reflects the performance metrics of full design space on the data path from top to bottom. The details of our simulation environment are described as follows:

\noindent \textbf{System configurations.}
We configure a typical handheld device system as our hardware platform for evaluation. This platform is same with VExpress V1 motherboard and equipped with multiple 64-bit mobile CPUs of ARMv8 ISA. For the full system simulation, we configure the kernel version as Linux Kernel 4.9.30, which provides the drivers for NVMe SSD and SCSI-based SSD. To accelerate the simulation speed, we configure the mobile CPU as atomic, which have been widely used in prior works.

\begin{table}[htp]
	\centering
	\caption{gem5 configuration}
	\label{eval_gem5_conf}
	\begin{tabular}{ll}
		\toprule
		gem5 parameters & Value                            \\ \midrule
		CPU             & ARMv8, Atomic CPU, 1$\sim$4 CPUs \\
		Machine         & VExpress\_GEM5\_V1               \\
		Linux Kernel    & Linux Kernel 4.9.30              \\ \bottomrule
	\end{tabular}
\end{table}

\noindent \textbf{Benchmarks.}
To evaluate the performance of different SSD types (i.e., NVMe SSD and SCSI SSD), we use a popular I/O performance benchmark \textit{fio}. \textit{fio} provides high flexibility to modify the factors that can impact I/O performance, such as the number of threads, the I/O engine, and the block size for each transaction, and queue depth. The details of \textit{fio} configurations are listed in Table \ref{eval_fio_conf}. Specifically, overall 512MB data is tested for each round of experiment and \textit{libaio} is used as I/O engine in the experiments to support asynchronous I/O accesses. To check how hardware and software configurations can impact the overall performance, we vary the number of CPU cores ranging from 1 to 4, the I/O queue depths ranging from 1 to 256, and the block size ranging from 4KB to 1MB. We collect the performance metrics for sequential read, sequential write, random read, and random write.

\begin{table}[htp]
	\centering
	\caption{FIO configuration}
	\label{eval_fio_conf}
	\begin{tabular}{ll}
		\toprule
		FIO parameters & Value        \\  \midrule
		bs             & 4K $\sim$ 1M \\
		ioengine       & libaio       \\
		iodepth        & 1 $\sim$ 256 \\
		iosize         & 512M         \\  \bottomrule
	\end{tabular}
\end{table}

\noindent \textbf{Mobile storage setup.}
Our new full system simulator is reconfigurable to change SSD internal resources. The details of SSD configurations in this experiment is listed in Table \ref{eval_simplessd_conf}. Specifically, the tested SSD consists of 4 channels and 1 NAND flash package (way) per channel. We adopt the datasheet from MICRON technology, which is widely used in market, for NAND flash configuration. Based on the datasheet, we configure 4 NAND flash dies per package, 2 planes per NAND flash die, 512 blocks per plane and 256 pages per block. In addition, the evaluated page size is 16KB, the NAND type is MLC, and communication interface follows ONFI 3.0. We also configure an internal DRAM-based read/write cache, which is 4-way set-associative.

\begin{table}[htp]
	\centering
	\caption{SimpleSSD configuration}
	\label{eval_simplessd_conf}
	\begin{tabular}{ll}
		\toprule
		SimpleSSD parameters & Value           \\ \midrule
		Channel              & 4               \\
		Way                  & 1               \\
		Die                  & 4               \\
		Plane                & 2               \\
		Block                & 512             \\
		Page                 & 256             \\
		Page size            & 16KB            \\
		NAND type            & MLC, ONFi 3.0   \\
		Cache                & 128 * 4 (256MB) \\ \bottomrule
	\end{tabular}
\end{table}

\begin{figure*}
	\subfloat[UFS sequential read.\label{fig:eval_fio_ufs_read}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_ufs_read.eps}
}
	\subfloat[NVMe sequential read.\label{fig:eval_fio_nvme_read}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_nvme_read.eps}
}
	\subfloat[UFS sequential write.\label{fig:eval_fio_ufs_write}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_ufs_write.eps}
}
	\subfloat[NVMe sequential write.\label{fig:eval_fio_nvme_write}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_nvme_write.eps}
}
	\caption{Overall performance of sequential I/O.\vspace{-15pt}}
	\label{fig:eval_fio_seq_perf}
\end{figure*}

\begin{figure*}
	\subfloat[UFS random read.\label{fig:eval_fio_ufs_randread}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_ufs_randread.eps}
	}
	\subfloat[NVMe random read.\label{fig:eval_fio_nvme_randread}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_nvme_randread.eps}
	}
	\subfloat[UFS random write.\label{fig:eval_fio_ufs_randwrite}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_ufs_randwrite.eps}
	}
	\subfloat[NVMe random write.\label{fig:eval_fio_nvme_randwrite}]{%
		\includegraphics[width=0.5\columnwidth]{figs/eval_fio_nvme_randwrite.eps}
	}
	\caption{Overall performance of random I/O.\vspace{-15pt}}
	\label{fig:eval_fio_rand_perf}
\end{figure*}

\begin{figure}[tp]
	\null\hfill
	\subfloat[UFS.\label{fig:eval_cpu_ufs}]{%
		\includegraphics[width=0.4\columnwidth]{figs/eval_cpu_ufs.eps}
	}
	\hfill
	\subfloat[NVMe.\label{fig:eval_cpu_nvme}]{%
		\includegraphics[width=0.4\columnwidth]{figs/eval_cpu_nvme.eps}
	}
	\hfill\null
	\caption{Performance behavior on multi-CPU.\vspace{-15pt}}
	\label{fig:eval_cpu_test}
\end{figure}


\begin{figure*}[tp]
	\null\hfill
	\subfloat[Bandwidth.\label{fig:eval_qtest_bw}]{%
		\includegraphics[width=0.85\columnwidth]{figs/eval_qtest_bw.eps}
	}
	\hfill
	\subfloat[Latency.\label{fig:eval_qtest_lat}]{
		\includegraphics[width=0.85\columnwidth]{figs/eval_qtest_lat.eps}
	}
	\hfill\null
	\caption{Performance behavior on I/O depth.\vspace{-15pt}}
	\label{fig:eval_qtest}
\end{figure*}

\subsection{Overall Performance}
Figures \ref{fig:eval_fio_seq_perf} and \ref{fig:eval_fio_rand_perf} show I/O access bandwidths of various block sizes and different cache options (i.e., without cache and with cache) for both UFS-based SSD and NVMe SSD we evaluated, which is collected from user space. Please note that the performance metrics we collect take into account the storage stack overhead and I/O data path latency. As shown in Figures \ref{fig:eval_fio_ufs_read} and \ref{fig:eval_fio_ufs_randread}, for read operations, the UFS interface becomes the bottleneck of UFS-based SSD, which constrains the overall storage to 1GB/s. In contrast, the PCIe interface and NVMe protocol provides much higher link bandwidth, which in turn enables the read performance of NVMe SSD to reach 2.8GB/s. On the other hand, as shown in Figures \ref{fig:eval_fio_ufs_write} and \ref{fig:eval_fio_ufs_randwrite}, the maximal write performance of UFS-based SSD with cache is around 750MB/s, which is not limited by maximal bandwidth of UFS interface. This is because, NAND Flash costs much longer time for write operations, which can significantly degrade the overall I/O performance. As shown in Figure \ref{fig:eval_fio_nvme_write}, the maximal write performance of NVMe SSD is 33\% higher than that of UFS-based SSD. This is because, NVMe SSD optimizes the software stack to minimize this impact on I/O access latency, which in turn can improve the overall I/O throughput. 

As shown in Figures \ref{fig:eval_fio_seq_perf} and \ref{fig:eval_fio_rand_perf}, SSD internal DRAM cache has great impact on I/O access performance, regardless of UFS-based SSD and NVMe SSD. For sequential I/O read access, SSD with cache achieves the maximal I/O bandwidth with less block sizes compared to SSD without cache, which is shown in Figures \ref{fig:eval_fio_ufs_read} and \ref{fig:eval_fio_nvme_read}. This is because, the internal DRAM cache employs read prefetch technique, which fetches large data volume from all NAND flash dies across SSD even though the I/O request size is much smaller than the SSD access volume. This technique can better utilize SSD internal parallelism, which in turn can improve I/O access performance. For the sequential write which is shown in Figures \ref{fig:eval_fio_ufs_write} and \ref{fig:eval_fio_nvme_write}, SSD with cache achieves the maximal I/O bandwidth with less block sizes compared to SSD without cache. In addition, the maximal sequential write bandwidths of SSD with cache are 25\% and 90\% higher than that of SSD without cache in UFS interface and NVMe interface, respectively. This is because, cache can accommodate the write requests and write it back to SSD at background, which in turn can hide the long latency of write operations. For random read, cache cannot improve the overall I/O performance, as requests in future cannot hit in the cache. On the other hand, for random write, its performance trend is similar to sequential write, thanks to the write cache.

\subsection{Multi-core Analysis}
Figure \ref{fig:eval_cpu_test} shows the performance of UFS-based SSD and NVMe SSD in terms of bandwidth under various number of CPU cores. As shown in the figure, NVMe SSD has bandwidth of 1GB/s with cache and the bandwidth of 22MB/s without cache regardless of the number of CPU cores. On the other hand, the bandwidth of UFS-based SSD degrades from 500MB/s to 300MB/s when increasing the number of CPU cores from 1 to 4, even though UFS-based SSD is equipped with cache. 

\subsection{I/O Queue Depth Analysis}
Figure \ref{fig:eval_qtest} shows the performance of UFS-based SSD and NVme SSD in terms of bandwidth and latency under various number of I/O queue depth. As shown in the figure, NVMe SSD can achieve great performance benefit from larger I/O queue depth, while larger I/O queue depth does not have significant impact on UFS-based SSD.

%% Evaluations
% Overall performance for seq. r/w rand. r/w
% Performance on multiple CPUs
% Performance on iodepth
